# BGS-WITSML Mapping Template #

### What is this repository for? ###

* Common place to store known BGS WITSML feed configuration files, espacially wits-map section, which can be used as template.

### How to use these template ###

* create a config file and use 
 ```
 include url("https://bitbucket.org/<account>/witsml-mapping/raw/HEAD/<filename>")
 ```